
package tcpclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TcpClient {

  
    public static void main(String[] args) {
        Socket s=null;
        
        try {
            int serverPort=2014;
            s=new Socket(args[1], serverPort);
            DataInputStream in=new DataInputStream(s.getInputStream());
            DataOutputStream out=new DataOutputStream(s.getOutputStream());
            out.writeUTF(args[0]);
            String data=in.readUTF();
            System.out.println("Recieved: "+data);
            
        }
        catch (UnknownHostException ex) {
            Logger.getLogger(TcpClient.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Host: "+ex.getMessage());
        }
        catch (IOException ex) {
            Logger.getLogger(TcpClient.class.getName()).log(Level.SEVERE, null, ex);
             System.out.println("IO:" +ex.getMessage());
        }
       
        finally { 
            if(s!=null) try {
                s.close();
            } catch (IOException ex) {
                Logger.getLogger(TcpClient.class.getName()).log(Level.SEVERE, null, ex);
                 System.out.println(ex.getMessage());
            }
        }
    }
}

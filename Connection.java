
package tcpserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Connection extends Thread{
    DataInputStream in;
    DataOutputStream out;
    Socket clientSocket;
    
    public Connection(Socket aclientSocket)
    {
        clientSocket=aclientSocket;
        try {
            in=new DataInputStream(clientSocket.getInputStream());
            out=new DataOutputStream(clientSocket.getOutputStream());
            this.start();
        }
        catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Connection:" +ex.getMessage());
        }
    }
    
    @Override
    public void run()
    {
        try {
            String data=in.readUTF();
           String []x = data.split(",");
           int[]arr=new int[x.length];
           int sum=0;
            for (int i = 0; i < arr.length ; i++)
            {
                arr[i] = Integer.parseInt(x[i]);
                sum  = sum + arr[i];
            }
  
            out.writeUTF(data+" sum = "+sum);
            System.out.println("Done...");
        }
        catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("IO: "+ex.getMessage());
        }
        
        finally { 
              try {
                clientSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(TcpServer.class.getName()).log(Level.SEVERE, null, ex);
                 System.out.println(ex.getMessage());
            }
        
  
    
              
           
}
    }
}



